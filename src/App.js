import React, { Component } from 'react'
import {
  BrowserRouter as Router, 
  Route, 
  Link, 
  Redirect,
  withRouter
} from 'react-router-dom'



class App extends Component {

  
  Public = () => {
    return (
      <div>
        Public page
      </div>
    )
  }

  Any = ({match}) => {
    return (
      <div>
        {match.params.id}
      </div>
    )
  }
  Login = () => {
    return <h1>Login page</h1>
  }

  Logout = () => {
    return (
      <button >Logout</button>
    )
  }

  Protected = () => {
    return (
      <div>
        <Link  to="/logout" component={this.Logout}/>
        <p>Protected page</p>
      </div>
    )
  }

  render() {
    return (
      <Router>

        <nav>
          <ul>
            <li>
              <Link  to = '/' > Home </Link> 
            </li>
            <li>
              <Link  to = '/public'> Public </Link> 
            </li>
            <li>
              <Link  to = '/protected'> Protected </Link> 
            </li>
          </ul>
          <Route exact path="/public" component = {this.APublicny} />
          <Route exact path="/login" component = {this.Login} />
          <PrivateRoute exact path="/:id" component = {this.Protected} />
          <Route exact path="/:id" component = {this.Any} />
        </nav>

       
      </Router>
    );
  }
}

export default App;
